<?php
namespace App\Error;

use Cake\Error\ExceptionRenderer;

class AppErrorRenderer extends ExceptionRenderer
{
	public function missingResourceFile($exception) {
		return jsonResponse($this->controller->getResponse(), $exception);
	}

	public function outOfBound($exception) {
		return jsonResponse($this->controller->getResponse(), $exception);
	}

	public function missingKey($exception) {
		return jsonResponse($this->controller->getResponse(), $exception);
	}
}

function jsonResponse($response, $exception) {
	return $response
		->withStatus($exception->getCode())
		->withType('json')
		->withStringBody(json_encode([
			'message' => $exception->getMessage()
		]));
}