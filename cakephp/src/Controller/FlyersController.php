<?php

namespace App\Controller;

use App\Controller\AppController;

class FlyersController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Flyer');
    }

    public function index()
    {
        $page = intval($this->request->getQuery('page') ?: "1");
        $limit = intval($this->request->getQuery('limit') ?: "100");
        $filter = $this->request->getQuery('filter') ?:  [];
        $this->set($this->Flyer->list($page, $limit, $filter));
        $this->viewBuilder()->setOption('serialize', true);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function read($id = 0)
    {
        $fields = $this->request->getQuery('fields');
        $this->set($this->Flyer->byId(
            $id,
            $fields ? explode(',', $fields) : null
        ));

        $this->viewBuilder()->setOption('serialize', true);
        $this->RequestHandler->renderAs($this, 'json');
    }
}
