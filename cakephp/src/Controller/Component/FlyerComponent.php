<?php
namespace App\Controller\Component;

use Cake\Controller\Component;

class FlyerComponent extends Component
{
    protected $flyers;

    function __construct(...$args)
    {
        parent::__construct(...$args);
        $path = APP . '../../' . getenv('SOURCE_PATH');
        $this->flyers = new FlyerIterator(fopen($path, 'r'));
        $this->flyers->pickColumns();
    }

    public function list($page = 1, $limit = 100, $filter = [])
    {
        $this->verifyFilters($filter);
        $this->flyers->setStrategy(FlyerIterator::filter($filter));
        return $this->flyers->listAll($page, $limit);
    }

    public function byId($id, $fields = null)
    {
        $this->flyers->setStrategy(FlyerIterator::filter(['id' => $id]));
        return Flyer::print($this->flyers->find(), $fields);
    }

    protected function verifyFilters($filter)
    {
        foreach($filter as $key => $value) {
            if(!in_array($key, ["category", "is_published"])){
                throw new MissingKeyException("$key is not a valid filter");
            }
        }
    }

    function __destruct()
    {
        if($this->flyers->file) {
            fclose($this->flyers->file);
        }
    }
}

class FlyerIterator implements \Iterator
{
    public $columns = [];
    public $file;

    public static function filter($filter)
    {
        return function($data) use ($filter) {
            foreach($filter as $key => $value) {
                if($data->$key != $value){
                    return false;
                }
            }
            return true;
        };
    }

    public function __construct($file, $handler = null)
    {
        $this->file = $file;
        $this->setStrategy($handler);
    }

    public function pickColumns()
    {
        $this->columns = fgetcsv($this->file);
    }

    protected function makeHandler($handler)
    {
        return function($data) use ($handler) {
            return intval($data->id) && call_user_func($handler, $data);
        };
    }

    public function setStrategy($handler = null)
    {
        $this->handler = $this->makeHandler($handler ?: function($data){
            return true;
        });
    }

    public function listAll($page = 1, $limit = 100)
    {
        $result = [];
        try {
            foreach($this as $key => $value) {
                if($key <= ($page - 1) * $limit) {
                    continue;
                }
                if($key > $page * $limit){
                    break;
                }
                if($value) {
                    $result []= Flyer::print($value);
                }
            }
        } catch (OutOfBoundException $e) {
            if(!count($result)){
                throw $e;
            }
        }
        return $result;
    }

    public function find()
    {
        $this->next();
        return $this->current();
    }

    public function rewind()
    {
        rewind($this->file);
        $this->position = 0;
    }

    public function current()
    {
        return $this->current;
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        while ($data = fgetcsv($this->file)) {
            $flyer = $this->makeFlyer(...$data);
            if($flyer && call_user_func($this->handler, $flyer, $this)){
                $this->current = $flyer;
                ++$this->position;
                return;
            }
        }
        $this->current = null;
        $this->position = -1;
        throw new OutOfBoundException("Index out of bound");
    }

    public function valid()
    {
        return $this->position != -1;
    }

    protected function makeFlyer($id, ...$tail)
    {
        if(intval($id)){
            return new Flyer($this->columns, $id, ...$tail);
        } else {
            return null;
        }
    }

    private $current = null;

    private $position = 0;
}

class Flyer {
    private $dict;
    private $columnList;
    function __construct($columns, ...$values) {
        $this->columnList = $columns;
        $this->dict = [];
        foreach($values as $key => $value){
            $this->dict[$columns[$key]] = $value;
        }
    }

    public function __get($prop)
    {
        return $this->dict[$prop];
    }

    public static function print($flyer, $opt = null)
    {
        $opt = $opt ?: $flyer->columnList;
        $result = [];
        foreach ($opt as $key) {
            if(array_key_exists($key, $flyer->dict)){
                $result[$key] = $flyer->dict[$key];
            } else {
                throw new MissingKeyException("$key is not a valid field");
            }
        }
        return $result;
    }
}

use \Cake\Http\Exception\NotFoundException;
use \Cake\Http\Exception\BadRequestException;

class OutOfBoundException extends NotFoundException
{
}

class MissingKeyException extends BadRequestException
{
}
