<?php
declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         1.2.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Test\TestCase\Controller;

use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * PagesControllerTest class
 *
 * @uses \App\Controller\PagesController
 */
class FlyerControllerTest extends TestCase
{
    use IntegrationTestTrait;
    /**
     * testMultipleGet method
     *
     * @return void
     */
    public function testMultipleGet()
    {
        $this->get('/flyers');
        $this->assertResponseOk();
        $this->get('/flyers/1');
        $this->assertResponseOk();
    }

    /**
     * testDisplay method
     *
     * @return void
     */
    public function testSingleFlyer()
    {
        $this->get('/flyers/1?fields=category,is_published');
        $this->assertResponseOk();
        $this->assertResponseContains('is_published');
        $this->assertResponseContains('category');
    }

    /**
     * testDisplay method
     *
     * @return void
     */
    public function testFilteredIndex()
    {
        $this->get('/flyers?page=2&limit=5&filter[is_published]=1&filter[category]=Iper+e+super');
        $this->assertResponseOk();
        $this->assertResponseContains('is_published');
        $this->assertResponseContains('category');
        $this->assertResponseContains('id');
    }

    /**
     * Test that missing template renders 404 page in production
     *
     * @return void
     */
    public function testMissingFlyer()
    {
        Configure::write('debug', false);
		$this->get('/flyers/0');
		$this->assertResponseCode(404);
        $this->assertResponseContains('message');
    }
    /**
     * Test that missing template renders 404 page in production
     *
     * @return void
     */
    public function testMissingFlyers()
    {
        Configure::write('debug', false);
		$this->get('/flyers?page=999');
		$this->assertResponseCode(404);
        $this->assertResponseContains('message');
    }

    /**
     * Test that missing template in debug mode renders missing_template error page
     *
     * @return void
     */
    public function testMissingField()
    {
        Configure::write('debug', true);
        $this->get('/flyers?filter[A]=0');
        $this->assertResponseCode(400);
        $this->assertResponseContains('message');
    }
}
