Flyer api
===

This CakePHP 4 project can serve the data in this [csv file](./flyer_data.csv), following this [specs](./2020_Istruzioni_TestCase_BackEndDevMarketplace.pdf)

Instructions
---
- Installation
	- clone the repo or download and unzip the project
	- go to the directory of the project
	- 
		```
		docker-compose build
		```

- Run the service
	```
	docker-compose up
	```

- Stop the service
	```
	docker-compose down
	```
- Install cakephp locally
	```
	cd cakephp && php ../composer.phar install --no-interaction
	```

- Run test (requires cakephp locally)
	```
	cd cakephp && vendor/bin/phpunit
	```

- Run the service in development mode (requires cakephp locally)
	```
	docker-compose -f ./docker-compose.dev.yml up
	```

Api
---
By default it serves on the port 4000 but the configuration can be changed in the docker-compose.yml file

- ```/flyers/{{id}}```: returns a single flyer
	- **fields** *int* default ```null```: field to show

- ```/flyers```: returns the list of flyers, accepts the following GET arguments
	- **page** *int* default ```1```: page number
	- **limit** *int* default ```100```: page size
	- **filter** *object* default ```null```: filter by values

the api interface is decribed more formally in the [openapi.yaml](./openapi.yaml)

Resources
---
- [flyers_data.csv](./flyers_data.csv): datasource
- [2020_Istruzioni_TestCase_BackEndDevMarketplace.pdf](./2020_Istruzioni_TestCase_BackEndDevMarketplace.pdf): specs
- [docker-compose.yml](./docker-compose.yml): builds environment
- [Dockerfile](./Dockerfile): makes the docker image
- [openapi.yaml](./openapi.yaml): OpenApi 3.0.0 spec

documentation
---
- **Component** file
	- *class* **FlyerComponent**: abstraction for buisness rules
		- *method* **list**: get a list of flyers. Accepts filters and pagination
		- *method* **byId**: get a single flyer. It is possible to display only some fields
	- *class* **FlyerIterator** extends Iterator: treat the resource file as an iterable
		- *method* **setStrategy**: set filtering algorithm
		- *method* **find**: find next flyer matching the rules
		- *method* **listAll**: list all the flyers matching the rules
	- *class* **Flyer**: abstraction for one entry
	- Exceptions:
		- *class* **OutOfBound**: http code 404
		- *class* **MissingKey**: http code 400
- **Controller** file
	- *class* **FlyersController**
		- has a ***FlyerComponent***
		- *action* **index**: handler for the flyer list
		- *action* **read**: handler for a single post

Note
---
The logic is handled in this file cakephp/src/Controller/Component/FlyerComponent.php.

The controller file is cakephp/src/Controller/FlyersController.php, it takes an instance of the component in order to query the data and then serve the response as json.

Errors are handled with custom exception classes defined in the component files and are rendered with the custom ErrorRenderer AppErrorRenderer,
defined in cakephp/src/Error/AppErrorRenderer.php.

The docker-compose.dev.yml builds the same service but adds a link to the current directory, for this reason is needed to run composer and install the cake project and the dependencies locally.
